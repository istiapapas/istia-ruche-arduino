#include <dht.h>
#include <aJSON.h>
#include <LowPower.h>
#include <SoftwareSerial.h>

//Define constant
#define RUCHE_ID 1 // ID of the Hive
#define DHT22_PIN 14 // Data Pin of the DHT Sensor
#define MAX_VALUES 24 // Number of values stored before sending
#define EMITTER_PIN 8 // Power pin of the Wireless Transmitter
#define TIME_BETWEEN_ACQUISITION 60 // in minutes


//Global Vars
SoftwareSerial mySerial(11, 10);
aJsonStream hf_stream(&mySerial);

// Arduino serial number
int arduinoSerial = 1;

// Sensors Define
double sensorsCount = 3;
int sensorsSerial[] = {
  1,2,3};
double **sensorsValue;
int beeCount;
dht DHT;

void setup(void)
{
  // Start software serial port
  mySerial.begin(4800);

  // Put emitter pin in output and low level
  pinMode(EMITTER_PIN,OUTPUT);
  digitalWrite(EMITTER_PIN, LOW);

  // Initialize fictive bee Count
  beeCount = 0;

  // Init value 2d array
  sensorsValue = (double **) (malloc(sensorsCount * sizeof(double)));
  for(int i = 0; i < sensorsCount; i++)
  {
    sensorsValue[i] = (double*) (malloc(MAX_VALUES * sizeof(double)));
  }
}

void loop(void)
{
  // Create Data object to send
  for(int i=0;i<MAX_VALUES;i++)
  {
  	// Pause before running the acquisition code (here 1 hour)
    pause(TIME_BETWEEN_ACQUISITION);  
    // Data acquisition
    DHT.read22(DHT22_PIN);
    // Data array
    sensorsValue[0][i] = DHT.temperature;
    sensorsValue[1][i] = DHT.humidity;
    sensorsValue[2][i] = beeCount++;
  }

  // JSON variables
  aJsonObject *root,*sensors,*sensor,*arduino,*values,*value;

  // Create and Fill JSON Object
  root=aJson.createObject(); 
  sensors = aJson.createArray();
  arduino = aJson.createObject();

  // For each sensor
  for(int i=0; i<sensorsCount; i++)
  {
  	// Local copies of data
    sensor = aJson.createObject(); 
    values = aJson.createArray();
    // Add sensor serial number
    aJson.addNumberToObject(sensor,"serial",sensorsSerial[i]);
    // Add each value 
    for(int j=0;j<MAX_VALUES;j++)
    {
      if(sensorsValue[i][j] != -999)
      {
        value = aJson.createItem(sensorsValue[i][j]);
        aJson.addItemToArray(values,value);
      }
    }
    // Store into the JSON Object
    aJson.addItemToObject(sensor, "values", values);
    aJson.addItemToObject(sensors,"sensors", sensor);

  }
  // Add sensors to JSON
  aJson.addItemToObject(root, "sensors", sensors);

  // Set arduino serial
  aJson.addNumberToObject(arduino, "serial", arduinoSerial);
  aJson.addItemToObject(root, "arduino", arduino);

  // PowerOn Emitter
  digitalWrite(EMITTER_PIN, HIGH);
  // Wait to be sure it started 
  delay(50);

  // Send JSON message
  aJson.print(root, &hf_stream);
  mySerial.println(); /* Add newline. */
  
  // Destroy Object
  aJson.deleteItem(root);

  // Wait for transmission complete
  mySerial.flush();

  // PowerOff Emitter
  delay(1000);
  digitalWrite(EMITTER_PIN, LOW);
}

void pause(double minute)
{
  //PAUSE X Min
  for(int j=0; j<(minute*60/8); j++)
  {
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  }
}







